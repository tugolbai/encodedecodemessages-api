import { Component, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { PostResponseService } from './services/post-response.service';
import { TextData } from './models/response.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent {
  @ViewChild('f') form!: NgForm;

  constructor(private responseService: PostResponseService) { }

  getEncoded() {
    const body: TextData = {
      password: this.form.value.password,
      message: this.form.value.decoded
    };

    this.responseService.getEncodedText(body).subscribe(response => {
      this.form.form.patchValue(response);
    })
  }

  getDecoded() {
    const body: TextData = {
      password: this.form.value.password,
      message: this.form.value.encoded
    };

    this.responseService.getDecodedText(body).subscribe(response => {
      this.form.form.patchValue(response);
    })
  }
}
