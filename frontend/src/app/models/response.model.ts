export class Encoded {
  constructor(
    public encoded: string,
  ) {
  }
}

export class Decoded {
  constructor(
    public decoded: string,
  ) {
  }
}

export interface TextData {
  password: string,
  message: string,
}
