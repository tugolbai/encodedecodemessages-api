import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Decoded, Encoded, TextData } from '../models/response.model';

@Injectable({
  providedIn: 'root'
})
export class PostResponseService {

  constructor(private http: HttpClient) { }

  getEncodedText(textData: TextData) {
    return this.http.post<Encoded>('http://localhost:8000/encode', textData);
  }

  getDecodedText(textData: TextData) {
    return this.http.post<Decoded>('http://localhost:8000/decode', textData);
  }
}
