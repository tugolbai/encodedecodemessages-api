const express = require('express');
const Vigenere = require('caesar-salad').Vigenere;
const cors = require('cors');

const app = express();
const port = 8000;

app.use(cors({origin: 'http://localhost:4200'}));
app.use(express.json());

app.post('/encode', (req, res) => {
    const text = {
        password: req.body.password,
        message: req.body.message,
    }
    return res.send({encoded: Vigenere.Cipher(text.password).crypt(text.message)});
});

app.post('/decode', (req, res) => {
    const text = {
        password: req.body.password,
        message: req.body.message,
    }
    return res.send({decoded: Vigenere.Decipher(text.password).crypt(text.message)});
});


app.listen(port, () => {
    console.log('We are live on ' + port);
});